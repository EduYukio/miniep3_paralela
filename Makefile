CC = gcc
FILES = pi_process.c
BIN = pi_process
CFLAGS = -g -lm -std=gnu99 -Wall -pthread

compile:
	$(CC) $(FILES) -o $(BIN) $(CFLAGS)

clean:
	rm -rf *.o $(BIN)