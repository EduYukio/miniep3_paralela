#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <math.h>
#include <semaphore.h>
#include <sys/mman.h>
#include <sys/wait.h>

struct shared {
	sem_t mutex;
	double sum; /* and the counter */
	double x;
	double deltaX;
};

void usage(){
	printf("usage: ./pi_process NUM_PROCESSOS NUM_PONTOS\nNUM_PROCESSOS > 0 and NUM_PONTOS > 1\n");
}

void riemman(struct shared* ptr){
	double dangerous_diff = 1 - pow(ptr->x, 2);
	if(dangerous_diff >= 0){
		ptr->sum += sqrt(dangerous_diff) * ptr->deltaX;
	}

	ptr->x += ptr->deltaX;
}

void spawn_proc(struct shared* ptr, int points){
	pid_t pid = fork();
	if (pid < 0) {
		munmap(ptr, sizeof(struct shared));
		perror("fork");
		exit(1);
	}
	if (pid == 0) {
		/* child */

		sem_wait(&ptr->mutex);

		for(int i = 0; i < points; i++){
			riemman(ptr);
		}

		sem_post(&ptr->mutex);
		exit(0);
	} 
}

int main(int argc, char** argv){
	if (argc != 3 || atoi(argv[1]) < 1 || atoi(argv[2]) < 2) {
		usage();
		return -1;
	}

	int num_processos = atoi(argv[1]);
	int num_pontos = atoi(argv[2]);

	struct shared* ptr;
	ptr = mmap(NULL, sizeof(struct shared), PROT_READ|PROT_WRITE, MAP_SHARED|MAP_ANONYMOUS, -1, 0);
	ptr->sum = 0;
	ptr->x = 0;
	ptr->deltaX = (double)1/(num_pontos - 1);

	double ratio = num_pontos/num_processos;
	int ceil_procs = 0;
	int floor_procs = 0;
	if(ratio > 1){
		ceil_procs = num_pontos - (int)floor(ratio)*num_processos;
		floor_procs = num_processos - ceil_procs;
	} else {
		ceil_procs = num_pontos;
	}

	sem_init(&ptr->mutex, 1, 1);
	setbuf(stdout, NULL);

	for(int j = 0; j < floor_procs; j++){
		spawn_proc(ptr, floor(ratio));
	}

	for(int k = 0; k < ceil_procs; k++){
		spawn_proc(ptr, ceil(ratio));
	}
	
	/* parent */
	for (int i = 0; i < num_processos; i++) {
		wait(NULL);
	}

	ptr->sum *= 4;
	printf("Pi Approximation: %f\n", ptr->sum);

	munmap(ptr, sizeof(struct shared));
	return 0;
}